#   Ejecutar la siguiente sentencia de asignacion, para expresiones determinadas
#   autor   =   "Michael Vasquez"
#   email   =   "michael.vasquez@unl.edu.ec"

ancho   =   17
alto   =   12.0


print("Utilizando", ancho, "de ancho y", alto, "de alto, obtenemos las siguientes expresiones" )

#   Operaciones internas
res1    =   ancho// 2
res2    =   ancho/ 2.0
res3    =   alto/ 3
res4    =   1+2*5

#   Lo que se muestra en pantalla
print("1. ancho/2= \n", res1)
print (type(res1))
print("\n2. ancho/2.0= \n", res2)
print (type(res2))
print("\n3. alto/3= \n", res3)
print((type(res3)))
print("\n1+2+5=\n", res4)
print(type(res4))